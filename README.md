# Filtrage Linéaire Optimal
## `News`

<!--


##### Suivi pédagogique
Voici le lien **zoom** pour suivre les séances en visioconférence si vous êtes tenu à l'isolement pour des *raisons sanitaires*  (ce lien est également indiqué sur la  [page chamilo](https://chamilo.grenoble-inp.fr/courses/PHELMA4PMSFIL9/index.php?) du cours) :
- https://grenoble-inp.zoom.us/j/99165253858
- *Passcode:* 281492Z

Dans ce cas, *merci de me prévenir également par mail* [florent.chatelain@grenoble-inp.fr](mailto:Florent.Chatelain@grenoble-inp.fr) *avant la séance.*
-->



<!--
#### Travail pour le Vendredi 28 janvier (10h30-12h30)
- Lire la fin des [slides](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/1_Wiener_non_causal.pdf) sur les exemples de filtrage inverse
 (pp. 23-29) et exécuter le notebook associé
[N1_denoising_chirp_Wiener_smoothing.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/1_Wiener_non_causal/N1_denoising_chirp_Wiener_smoothing.ipynb), et répondre aux questions suivantes *de manière synthétique* :
  1. Le débruitage obtenu vous semble t'il satisfaisant ?
  2. Comment interpréter le gain (fonction de tranfert) du filtre de Wiener non causal tracé à la cellule [19] ?
  3. Quel serait ici l'avantage du filtre de Wiener par rapport à un simple filtre passe bande ?
  4. Quelle hypothèse fondamentale requise par le filtre de Wiener n'est ici pas respectée ? Que conclure ?
- **Déposer vos réponses** sous la forme d'un **document pdf** (export d'un éditeur de texte ou scan pdf de vos notes manuscrites) via
cette [tâche Chamilo](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA4PMSFIL9&id_session=0&gidReq=0&gradebook=0&origin=&id=221412&isStudentView=true)
-->



#### Séance 7 : Vendredi 12 avril
- Fin de la leçon [5_Kalman.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/5_Kalman.pdf)
sur le filtre de Kalman.
- BE : illustration du filtrage de Kalman pour le suivi de trajectoire d'une fusée au décollage,   notebook
[N8_kalman_suivi_fusee.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/5_Kalman/N8_kalman_suivi_fusee.ipynb).

#### Séance 6 : Mardi 9 avril
- Fin de la leçon [4_filtrage_adaptatif.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/slides/4_filtrage_adaptatif.pdf) : filtrage adaptatif LMS
- BE : illustrations du filtrage adaptatif sur différents problèmes/notebooks
  - [N6_annulation_echo.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/4_filtrage_adaptatif/N6_annulation_echo.ipynb) sur l'annulation d'écho pour la *téléphonie main libre*
  - [N7_ecg_foetal-template.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/4_filtrage_adaptatif/N7_ecg_foetal-template.ipynb) sur l'expérience de Widrow afin d'estimer l'ECG du foetus chez la femme enceinte
-  Début de la  leçon [5_Kalman.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/slides/5_Kalman.pdf) sur le filtre de Kalman

#### ~~Séance 5 : Mardi 2 avril (8h15-10h15)~~

- ~~slides [4_filtrage_adaptatif.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/slides/4_filtrage_adaptatif.pdf) : filtrage adaptatif RLS~~
- ~~TD : notion d'adaptativité (exemple de l'estimateur récursif de la moyenne)~~

#### ~~Séance 4 : Vendredi 29 mars (10h30-12h30)~~

- ~~slides [3_Wiener_discret-modeles_AR.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/3_Wiener_discret-modeles_AR.pdf) sur le filtre de Wiener discret et les modèles AR.~~
- ~~BE : application du filtre de Wiener auto-ajusté pour débruiter les signaux EMG, notebook [N3_denoising_EMG-template.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N3_denoising_EMG-template.ipynb)~~
- ~~BE : analyse/synthese de parole à l'aide de processus AR~~ 
  - ~~modélisation de sons voisés, notebook [N4_analyse_voyelle.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N4_analyse_voyelle.ipynb)~~
  - ~~vocoder LPC10, notebook [N5_vocoder_lpc.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N5_vocoder_lpc.ipynb)~~

#### ~~Travail pour Vendredi 29 mars~~

~~Terminer l'exercice d'application du filtre de Wiener causal :~~

  $`\left\{ \begin{array}{rcl} Y(t) & = & X(t) + V(t)\\ S(t) & = & \dot{X}(t) \quad \textrm{ (dérivée de } X(t))\end{array} \right.`$ 

  ~~avec les mêmes hypothèses que l'exercice vu en cours (cf. séance 3 ci-dessous ) : $X$ et $V$ décorrélés et~~

  $`\left\{ \begin{array}{rcl} \gamma_{XX}(\nu) & = & \frac{5}{(\nu^2+4)^2}\\ & & \\ \gamma_{VV}(\nu) & = & \frac{1}{(\nu^2+4)} \end{array} \right.`$

- ~~Trouver la réponse en fréquence et proposer un schéma de principe pour le filtrage (analogique) : montage série/parallèle pour obtenir l'estimée de $S$ à partir de $Y$
(**indice** : on a montré en cours que dans ce cas $\gamma_{SY}(\nu)=2i\pi\nu\gamma_{XX}(\nu)$).~~
- ~~**Déposer vos réponses** sous la forme d'un **document pdf** (export d'un éditeur de texte ou scan pdf de vos notes manuscrites) via
cette [tâche Chamilo](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA4PMSFIL9&id_session=0&gidReq=0&gradebook=0&origin=&id=221412&isStudentView=true)~~


#### ~~Séance 3 : Vendredi 22 mars (13h30-15h30)~~

- ~~fin de la leçon [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf) sur la synthèse du filtre causal.~~
- ~~TD : exercice d'application du filtre de Wiener causal au débruitage:~~

  $`\left\{ \begin{array}{rcl} Y(t) & = & X(t) + V(t)\\ S(t) & = & X(t) \end{array} \right.`$

 ~~avec $X$ et $V$ décorrélés et~~

  $`\left\{ \begin{array}{rcl} \gamma_{XX}(\nu) & = & \frac{5}{(\nu^2+4)^2}\\ & & \\ \gamma_{VV}(\nu) & = & \frac{1}{(\nu^2+4)} \end{array} \right.`$

 - ~~Trouver la réponse en fréquence et proposer un schéma de principe pour le filtrage (analogique) : montage série/parallèle pour obtenir l'estimée de S à partir de Y.~~
 - ~~Version discrète du filtre de Wiener causal et anti-causal (fin de la leçon [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf)).~~



#### ~~Travail pour vendredi 22 mars (13h30-15h30)~~

- ~~Lire (et comprendre) les slides [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf) p. 11-14 sur la factorisation spectrale et la synthèse du filtre blanchisseur causal et causalement inversible.~~


####  ~~Séance 2 : Mardi 19 mars (10h30-12h30)~~
 - ~~fin exercice de débruitage détaillé sur les [slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/1_Wiener_non_causal.pdf) p.22~~
 - ~~BE: applications en filtrage inverse~~
   - ~~notebook [N1_denoising_chirp_Wiener_smoothing.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/notebooks/1_Wiener_non_causal/N1_denoising_chirp_Wiener_smoothing.ipynb)~~
   - ~~notebook [N2_deconvolution_image_Wiener_smoothing.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/notebooks/1_Wiener_non_causal/N2_deconvolution_image_Wiener_smoothing.ipynb)~~
 - ~~slides [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf) : filtre de Wiener **causal** et approche de Bode-Shannon, jusqu'au théorème de Paley-Wiener *slide 10*~~

#### ~~Travail pour le mardi 19 mars (10h30-12h30)~~

- ~~Terminer l'exercice de débruitage détaillé sur les [slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/1_Wiener_non_causal.pdf) p.22 : Donner l'expression de gain complexe $`G_{\textrm{NC}}(\nu)`$ du filtre optimal et de la densité spectrale de l'erreur $`\gamma_{EE}(\nu)`$ dans les deux cas limites suivants:~~
  1. $\gamma_{VV}(\nu) \rightarrow 0$
  2. $\gamma_{VV}(\nu) \rightarrow +\infty$
  
  <!--**Déposer vos réponses** sous la forme d'un **document pdf** (export d'un éditeur de texte ou scan pdf de vos notes manuscrites) via ce  [répertoire partagé](https://cloud.univ-grenoble-alpes.fr/s/TPD6T5LKktETZad) (dépot uniquement).-->
- ~~Lire la fin des [slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/1_Wiener_non_causal.pdf) sur les exemples de filtrage inverse (pp. 23-29) et exécuter le notebook associé [N1_denoising_chirp_Wiener_smoothing.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/notebooks/1_Wiener_non_causal/N1_denoising_chirp_Wiener_smoothing.ipynb)~~

#### ~~Séance 1 : Lundi 18 mars (8h)~~

 - ~~slides [1_Wiener_non_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/1_Wiener_non_causal.pdf) : introduction + filtre de Wiener non causal + exemples/applications~~
 - ~~TD : exercice slide 22~~

## Bienvenue dans le cours de Filtrage Optimal Sicom !

Vous trouverez dans ce repo gitlab le matériel nécessaire à l'enseignement de *Filtrage optimal* :
  - supports de cours ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides))
  - exemples et exercices sous forme de [notebooks python](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/notebooks) (fichiers `.ipynb`).
Ces ressources seront actualisées en fonction de l'avancement des séances.


### Comment utiliser les notebooks Python ?

Les exemples et exercices se feront sous python 3.x à travers [scipy](https://www.scipy.org/) et également [scikit-learn](https://scikit-learn.org/).
Ces bibliothèques et modules sont parmi les plus utilisés actuellement pour le calcul numérique et la science des données.

Les *Jupyter Notebooks*  (fichiers `.ipynb`) sont des programmes contenant à la fois des cellules de code (pour nous Python)
et du texte en markdown pour le côté narratif.
Ces notebooks sont souvent utilisés pour explorer et analyser des données. Leur traitement se fait avec une application `jupyter-notebook`, ou `juypyter-lab`, à laquelle on accède par son navigateur web.

Afin de de pouvoir les exécuter vous avez au moins trois possibilités :

1. Téléchargez les notebooks pour les exécuter sur votre machine. Cela requiert d'avoir installé un environnement Python (> 3.3), et les packages Jupyter notebook et scikit-learn. On recommande de les installer via la <a href="https://www.anaconda.com/downloads">distribution Anaconda</a> qui installera directement toutes les dépendances nécessaires.

**Ou**

2. Utiliser un service en ligne `jupyterhub` :

  - je vous recommande celui de l'UGA [gricad-jupyter.univ-grenoble-alpes.fr](https://gricad-jupyter.univ-grenoble-alpes.fr)  afin que vous puissiez faire tourner les _notebooks_ sur le serveur de calcul de l'UGA tout en sauvegardant vos modifications et vos résultats. Également utile pour lancer un calcul en arrière-plan (connexion avec votre compte Agalan ; nécessite le téléversement _notebooks_+données sur le serveur).
  - Vous pouvez également utiliser un service `jupyterhub` équivalent. Par exemple, celui de google, à savoir [google-colab](https://colab.research.google.com/), qui vous permet d'exécuter/enregistrer vos ordinateurs portables et aussi de _partager l'édition à plusieurs collaborateurs_ (nécessite un compte google et le téléchargement de vos ordinateurs portables+données dans votre _Drive_)

**Ou**

3.  Utiliser le service et les liens _mybinder_ pour les exécuter de manière interactive et à distance (en ligne) : [ ![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fds-courses%2Foptimal-filtering-sicom/master?urlpath=lab/tree/notebooks) (ouvrez le lien et attendez quelques secondes que l'environnement se charge).<br>
  **Attention:** Ces _Binder_ sont destinés au codage interactif _ephemère_, ce qui signifie que vos propres modifications/codes/résultats seront perdus lorsque votre session utilisateur s'arrêtera automatiquement (en partique après 10 minutes d'inactivité)


**Note :**  Vous trouverez également parmi les notebooks une introduction à Python [notebooks/0_python_in_a_nutshell](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/notebooks%2F0_python_in_a_nutshell)
